﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Complete
{
    public class TankShooting : NetworkBehaviour
    {
        public int m_PlayerNumber = 1;              // Used to identify the different players.
        public Rigidbody m_Shell;                   // Prefab of the shell.
        public Transform m_FireTransform;           // A child of the tank where the shells are spawned.
        public Slider m_AimSlider;                  // A child of the tank that displays the current launch force.
        public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
        public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
        public AudioClip m_FireClip;                // Audio that plays when each shot is fired.
        public float m_MinLaunchForce = 15f;        // The force given to the shell if the fire button is not held.
        public float m_MaxLaunchForce = 30f;        // The force given to the shell if the fire button is held for the max charge time.
        public float m_MaxChargeTime = 0.75f;       // How long the shell can charge for before it is fired at max force.
        
        private string m_FireButton;                // The input axis that is used for launching shells.      
        
        private float m_CurrentLaunchForce;         // The force that will be given to the shell when the fire button is released.
        private float m_ChargeSpeed;                // How fast the launch force increases, based on the max charge time.
        private bool m_Fired;                       // Whether or not the shell has been launched with this button press.

        private List<WeaponType> weaponList;

        private void OnEnable()
        {
            weaponList = new List<WeaponType>();
            weaponList.Add(WeaponType.OneShot);

            // When the tank is turned on, reset the launch force and the UI
            m_CurrentLaunchForce = m_MinLaunchForce;
            m_AimSlider.value = m_MinLaunchForce;
        }


        private void Start()
        {
            // The fire axis is based on the player number.
            m_FireButton = "Fire" + m_PlayerNumber;

            // The rate that the launch force charges up is the range of possible forces by the max charge time.
            m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
        }


        private void Update()
        {
            if (!isLocalPlayer)
            {
                return;
            }

            // The slider should have a default value of the minimum launch force.
            m_AimSlider.value = m_MinLaunchForce;

            // If the max force has been exceeded and the shell hasn't yet been launched...
            if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
            {
                m_Fired = true;
                // ... use the max force and launch the shell.
                m_CurrentLaunchForce = m_MaxLaunchForce;
                m_ShootingAudio.Stop();
                //Debug.Log("MaximumLaunch " + m_CurrentLaunchForce);
                CmdFire(m_CurrentLaunchForce);
            }
            // Otherwise, if the fire button has just started being pressed...
            else if (Input.GetButtonDown(m_FireButton))
            {
                //Debug.Log("GetFirstButtonDown");
                // ... reset the fired flag and reset the launch force.
                m_Fired = false;
                m_CurrentLaunchForce = m_MinLaunchForce;

                // Change the clip to the charging clip and start it playing.
                m_ShootingAudio.clip = m_ChargingClip;
                m_ShootingAudio.Play();
            }
            // Otherwise, if the fire button is being held and the shell hasn't been launched yet...
            else if (Input.GetButton(m_FireButton) && !m_Fired)
            {
                //Debug.Log("HoldDown");
                // Increment the launch force and update the slider.
                m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;
                m_AimSlider.value = m_CurrentLaunchForce;
            }
            // Otherwise, if the fire button is released and the shell hasn't been launched yet...
            else if (Input.GetButtonUp(m_FireButton) && !m_Fired)
            {
                //Debug.Log("ButtonUp " + m_CurrentLaunchForce);
                m_ShootingAudio.Stop();
                // ... launch the shell.
                CmdFire(m_CurrentLaunchForce);
            }
        }

        [Command]
        private void CmdFire(float launchForce)
        {
            // Set the fired flag so only Fire is only called once.
            m_Fired = true;

            // Create an instance of the shell and store a reference to it's rigidbody.
            Rigidbody shellInstance =
                Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

            // Set the shell's velocity to the launch force in the fire position's forward direction.
            shellInstance.velocity = launchForce * m_FireTransform.forward;

            // Change the clip to the firing clip and play it.
            m_ShootingAudio.clip = m_FireClip;
            m_ShootingAudio.Play();

            // Reset the launch force.  This is a precaution in case of missing button events.
            m_CurrentLaunchForce = m_MinLaunchForce;

            // Spawn the bullet on all the Clients;
            NetworkServer.Spawn(shellInstance.gameObject);
        }


        //private void Fire2()
        //{
        //    Debug.Log("Fire");
        //    // Set the fired flag so only Fire is only called once.
        //    m_Fired = true;

        //    // Create an instance of the shell and store a reference to it's rigidbody.
        //    Rigidbody shellInstance =
        //        Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        //    // Set the shell's velocity to the launch force in the fire position's forward direction.
        //    shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;

        //    // Change the clip to the firing clip and play it.
        //    m_ShootingAudio.clip = m_FireClip;
        //    m_ShootingAudio.Play();
        //    //Rigidbody shellInstance =
        //    //    Instantiate (m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        //    // Set the shell's velocity to the launch force in the fire position's forward direction.
        //    //shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;

        //    FireVisualClientShell();//(shellInstance.velocity, m_FireTransform.position);
        //    //// Change the clip to the firing clip and play it.
        //    //m_ShootingAudio.clip = m_FireClip;
        //    //m_ShootingAudio.Play ();

        //    CmdFire();//(shellInstance.velocity, m_FireTransform.position);

        //    // Reset the launch force.  This is a precaution in case of missing button events.
        //    m_CurrentLaunchForce = m_MinLaunchForce;
            
        //    // Spawn the bullet on the Clients;
        //    // NetworkServer.Spawn(shellInstance.gameObject);
        //}
        
        //// Called by the client to tell the server it has fired
        //[Command]
        //private void CmdFire2()//(Vector3 shotVector, Vector3 position)
        //{
        //    Debug.Log("CmdFire");
        //    RpcFire(m_PlayerNumber);//, shotVector, position);
        //}

        //// Called by the server to tell clients that a tank has fired
        //[ClientRpc]
        //private void RpcFire(int playerId)//, Vector3 shotVector, Vector3 position)
        //{
        //    //if (playerId != TankShooting.s_LocalTank.m_PlayerNumber)
        //    //{
        //    //    Debug.Log("Entering here");
        //    //    NetworkServer.Spawn(FireVisualClientShell().gameObject);//(shotVector, position);
        //    //}
        //    Debug.Log("Entering RpcFire");
        //}
        
        //// Takes care of all the aesthetic elements of firing
        //private Rigidbody GetShellType()
        //{
        //    Rigidbody shellType = m_Shell;
            
        //    return shellType;
        //}

        //private Rigidbody FireVisualClientShell()//(Vector3 shotVector, Vector3 position)
        //{
        //    Debug.Log("FireVisualClientShell");
        //    Rigidbody shellInstance =
        //        Instantiate (m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        //    //shellInstance.transform.position = position;
        //    shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;

        //    Physics.IgnoreCollision(shellInstance.GetComponent<Collider>(), GetComponentInChildren<Collider>(), true);

        //    //// Change the clip to the firing clip and play it.
        //    m_ShootingAudio.clip = m_FireClip;
        //    m_ShootingAudio.Play();

        //    return shellInstance;
        //}

        //[ClientRpc]
        //private void RpcTest()
        //{
        //    // Change the clip to the firing clip and play it.
        //    m_ShootingAudio.clip = m_FireClip;
        //    m_ShootingAudio.Play();
        //    Debug.Log("Entering RpcTest");
        //    m_Fired = true;
        //}

        //public void AddWeapon(WeaponType item)
        //{
        //    foreach (var weapon in weaponList)
        //    {
        //        if (weapon == item) return;
        //    }
        //    weaponList.Add(item);
        //}
    }
}