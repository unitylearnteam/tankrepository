﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    public float speed = 1.0f;
    public Vector3 axis;

    void Update()
    {
        float addToAngle = 0.0f;
        transform.Rotate(axis, addToAngle + speed);
    }
}
