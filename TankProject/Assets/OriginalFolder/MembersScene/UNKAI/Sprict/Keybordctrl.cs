﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keybordctrl : MonoBehaviour
{
    //button
    GameObject single, online, quit;
    int a = 0;
    void Start()
    {
        single = GameObject.Find("Single");
        online = GameObject.Find("Online");
        quit = GameObject.Find("Quit");
        //highlight
        single.GetComponent<UnityEngine.UI.Button>().Select();

    }

    void Update()
    {
        //選択メニュー
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            a--;
            if (a < 0) a = 2;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            a++;
        }
        switch (a % 3)
        {
            case 0:
                single.GetComponent<UnityEngine.UI.Button>().Select();
                break;
            case 1:
                online.GetComponent<UnityEngine.UI.Button>().Select();
                break;
            case 2:
                quit.GetComponent<UnityEngine.UI.Button>().Select();
                break;
        }
    }
}
