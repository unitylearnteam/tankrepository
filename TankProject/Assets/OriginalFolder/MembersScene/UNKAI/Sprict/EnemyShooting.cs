﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyShooting : MonoBehaviour
{

    public Rigidbody m_Shell;                   // Prefab of the shell.
    public Transform m_FireTransform;           // A child of the tank where the shells are spawned.

    public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
    public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
    public AudioClip m_FireClip;                // Audio that plays when each shot is fired.
    private GameObject player;
    public float attackTime, attackTimer;

    void Start()
    {
        player = GameObject.Find("Cube");
        attackTimer = 0;
        attackTime = 2.0f;
    }
    void Update()
    {

        if (attackTimer > 0)
            attackTimer -= Time.deltaTime;
        if (attackTimer < 0)
            attackTimer = 0;
        float distance = Vector3.Distance(transform.position, player.transform.position);
        if (distance <= 10&&attackTimer==0)
        {
            Fire();
            attackTimer = attackTime;
        }


    }
    public void Fire()
    {
        Rigidbody shellInstance =
            Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
        shellInstance.velocity = 20.0f * m_FireTransform.forward;
        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();
    }
    void OnTriggerEnter(Collider c)
    {

        if (c.gameObject.layer == 10)
        {
            Item p = c.GetComponent<Item>();
            switch (p.item_type)
            {
                case 1:
                    
                    break;
            }
        }
    }
}
