﻿using UnityEngine;
using UnityEngine.AI;
public class EnemyTank : MonoBehaviour
{
    NavMeshAgent nav;
    private GameObject player;
    float distance;
    int nowstate;
    bool isRandom;
    float stoptime;
    bool isChangestate;
    float x;
    float z;
    void Start()
    {
        player = GameObject.Find("Cube");
        nav = gameObject.GetComponent<NavMeshAgent>();
        nowstate = 0;
        isRandom = true;
        stoptime = 0;
        isChangestate = true;
    }
    void Update()
    {
        distance = Vector3.Distance(transform.position, player.transform.position);
        distancechange();
        statechange();
    }
    void distancechange()
    {
        if (distance <= 20)
        {
            isChangestate = true;
            nowstate = 2;
        }
        else if (distance > 20)
        {
            if (isChangestate == true)
            {
                nowstate = 0;
                stoptime = 0;
                isChangestate = false;
            }
        }
    }
    void randomPosition()
    {
        x = Random.Range(-40, 40);
        z = Random.Range(-40, 40);
    }
    void statechange()
    {
        if (nowstate == 0)
        {
            nav.isStopped = true;
            stoptime += Time.deltaTime;
            isRandom = true;
            if (stoptime >= 2)
            {
                nowstate = 1;
            }
        }
        else if (nowstate == 1)
        {
            nav.isStopped = false;
            if (isRandom == true)
            {
                InvokeRepeating("randomPosition", 0, 10);
                isRandom = false;
            }
            Vector3 nextpos = new Vector3(x, transform.position.y, z);
            nav.destination = nextpos;
            Debug.DrawLine(transform.position, nextpos, Color.red);
            if (nav.remainingDistance <= 0.1f)
            {
                nowstate = 0;
                stoptime = 0;
            }
        }
        else if (nowstate == 2)
        {
            nav.destination = player.transform.position;
            if (nav.remainingDistance > 10)
            {
                nav.isStopped = false;
            }
            else if (nav.remainingDistance <= 10)
            {
                transform.LookAt(player.transform);
                nav.isStopped = true;
            }
               
              
        }
        Debug.DrawLine(transform.position, player.transform.position, Color.red);
    }
 

}

