﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankWeapon : MonoBehaviour
{
    public int m_PlayerNumber = 1;

    private List<WeaponType> weaponList;
    private int currentWeapon;

    private void OnEnable()
    {
        weaponList = new List<WeaponType>();
        weaponList.Add(WeaponType.OneShot);
        currentWeapon = 0;
    }

    private void Update()
    {
        if (weaponList.Count == 1) return;
        if (Input.GetKeyDown(KeyCode.Z))
        {
            ++currentWeapon;
            currentWeapon %= weaponList.Count;
        }
    }

    //アイテム用
    public void AddWeapon(WeaponType item)
    {
        foreach (var weapon in weaponList)
        {
            if (weapon == item) return;
        }
        weaponList.Add(item);
    }

    public WeaponType GetCurrentWeapon()
    {
        return weaponList[currentWeapon];
    }
}
