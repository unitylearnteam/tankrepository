﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ItemType
{
    //Itemの列挙
    HP_RECOVERY,
    SPEED_UP,
    //etc,

    //Weaponの列挙
    NORMAL_WEAPON,

    //なし
    NONE,
}

[System.Serializable]public struct SelectKey
{
    public KeyCode up;
    public KeyCode down;
    public KeyCode decide;
}

[System.Serializable]
public class ItemPair
{
    public ItemType type;
    public Image uiImage;
    public int num;
}

public class ItemUIList : MonoBehaviour
{
    //どの列挙とどのUIプレハブが対応するかを設定する(設定用)
    [SerializeField]
    private List<ItemPair> itemList;

    [Range(0.0f,1.0f)]
    [SerializeField]
    private float reduceAlpha = 0.5f;

    [Header("選択中のものからどのくらいずらすか")]
    [SerializeField]
    private Vector3 shift;

    [Header("選択が早くなる範囲(単位秒)")]
    [SerializeField]private Utility.Range changeTimeRange;

    [Header("選択のスピードアップを抑える値")]
    [SerializeField]private float saveSpeedUp = 0.1f;

    [SerializeField]
    private SelectKey keys;

    //中にImageのComponentを持っているUIオブジェクト
    [SerializeField]private GameObject baseUIObject;

    //親のGameObject
    [SerializeField]
    private GameObject parentPlayer;

    //現在の選択切り替え時間
    private float currentChangeTime;

    //現在切り替えができるかどうか
    private bool isCanChange;

    //現在の添え字
    private int currentIndex;

    //実際の描画される3つのUI
    private GameObject[] drawUIs;

	// Use this for initialization
	void Start ()
    {
        currentChangeTime = changeTimeRange.max;

        currentIndex = 0;

        ItemListInit();

        //アイテムの切り替えができるようにする
        isCanChange = true;

        DrawUISetting();
	}

    private void ItemListInit()
    {
        foreach(var item in itemList)
        {
            item.num = 0;
        }
    }

    private void DrawUISetting()
    {
        drawUIs = new GameObject[3];

        for(int i = 0; i < 3; i++)
        {
            drawUIs[i] = Instantiate(baseUIObject);
            drawUIs[i].transform.parent = transform;
        }

        RectTransform myRectTrans = GetComponent<RectTransform>();

        rect(drawUIs[1]).position = myRectTrans.position;
        rect(drawUIs[0]).position = myRectTrans.position + shift;
        rect(drawUIs[2]).position = myRectTrans.position + Vector3.Scale(new Vector3(1, -1, 1), shift);
    }

    private RectTransform rect(GameObject obj)
    {
        return obj.GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //選択中の添え字の更新
        IndexUpdate();

        //currentIndexの値が範囲内になるように補正
        IndexControl();

        //表示するUIについて更新する
        UIUpdate();

        //アイテムの使用をチェックした上で行う
        Utility.CustomFunc.CheckAction(() => { return Input.GetKeyDown(keys.decide); }, () => UseItem(SelectType()));
	}

    public void AddItem(ItemType itemType)
    {
        //指定された列挙のアイテムが登録されたなかったらreturn
        if (!IsContainItem(itemType, itemList)) return;

        //アイテム数を1つ増やす
        //CallFunc(itemType, (ItemPair pair) => { pair.num++; });
        CallFunc(itemType, (p) => p.num++);
    }

    private void UseItem(ItemType itemType)
    {
        ItemPair item = SameItemPair(itemType);

        //使用しようとしているアイテムが登録されてない、またはアイテムを持っていなかったらreturn
        if (item == null) return;
        if (!IsHasItem(item)) return;

        //アイテムの数を減らす
        item.num--;

        //0未満にならないようにする
        item.num = Mathf.Max(item.num, 0);

        //アイテムのタイプに対応した関数を呼ぶ
        CallItemFunc(itemType);
    }

    private void CallItemFunc(ItemType itemType)
    {
        switch (itemType)
        {
            case ItemType.HP_RECOVERY:
                parentPlayer.GetComponent<Complete.TankHealth>().RecoveryItem();
                break;

            case ItemType.SPEED_UP:
                parentPlayer.GetComponent<Complete.TankMovement>().SpeedUpItem();
                break;

            case ItemType.NORMAL_WEAPON:
                //callfunc_currentnone
                break;

            case ItemType.NONE:
            default:
                break;
        }
    }

    private void CallFunc(ItemType itemType, System.Action<ItemPair> call)
    {
        ItemPair item = SameItemPair(itemType);
        if (item == null) return;

        call(item);
    }

    private ItemPair SameItemPair(ItemType itemType)
    {
        foreach(var pair in itemList)
        {
            if (pair.type == itemType) return pair;
        }

        return null;
    }

    //現在添え字が選択中のアイテムタイプ
    private ItemType SelectType()
    {
        return itemList[currentIndex].type;
    }

    //引数のアイテムタイプを保持しているかどうか
    private bool IsContainItem(ItemType itemType, List<ItemPair> pairList)
    {
        foreach(var pair in pairList)
        {
            if (pair.type == itemType) return true;
        }

        return false;
    }

    private void IndexUpdate()
    {
        if (IsItemChange(keys.up))
        {
            if (isCanChange)
            {
                IndexUpdate(-1);
                IndexChangeProcess();
            }
            ChangeSpeedUp();
            return;
        }

        if (IsItemChange(keys.down))
        {
            if (isCanChange)
            {
                IndexUpdate(1);
                IndexChangeProcess();
            }
            ChangeSpeedUp();
            return;
        }

        //indexの更新がなかったら切り替える時間をリセットする
        ResetChangeTime();
    }

    private void IndexChangeProcess()
    {
        isCanChange = false;
        StartCoroutine(Utility.Coroutine.DelayMethod(currentChangeTime, OnCanChange));
    }

    private void ChangeSpeedUp()
    {
        currentChangeTime -= 0.01f * saveSpeedUp;
        currentChangeTime = Mathf.Min(currentChangeTime, changeTimeRange.min);
    }

    private void ResetChangeTime()
    {
        currentChangeTime = changeTimeRange.max;
    }

    private void IndexUpdate(int add)
    {
        currentIndex += add;
    }

    //アイテムを切り替えるタイミングかどうか
    private bool IsItemChange(KeyCode key)
    {
        return Input.GetKey(key) || Input.GetKeyDown(key);
    }

    private void OnCanChange()
    {
        isCanChange = true;
    }

    private void IndexControl()
    {
        int maxIndex = MaxIndex();

        if (currentIndex > maxIndex)
        {
            currentIndex = 0;
            return;
        }

        if (currentIndex < 0)
        {
            currentIndex = maxIndex;
        }
    }

    private int MaxIndex()
    {
        int maxIndex = itemList.Count - 1;

        if (maxIndex < 0) return 0;
        return maxIndex;
    }

    private void SettingUIInfo(int listIndex, int drawUIIndex, float alpha)
    {
        Image baseImage = itemList[listIndex].uiImage;

        Image drawImage = drawUIs[drawUIIndex].GetComponent<Image>();
        drawImage.sprite = baseImage.sprite;

        //色を更新
        Color color = baseImage.color;
        color.a = alpha;
        drawImage.color = color;

        //所持数を更新
        Text numText = drawUIs[drawUIIndex].GetComponentInChildren<Text>();
        string writeStr = "x";
        writeStr += itemList[listIndex].num;

        numText.text = writeStr;
    }

    private int NextIndex(int currentIndex)
    {
        currentIndex++;
        int maxIndex = MaxIndex();
        if (currentIndex > maxIndex) return 0;

        return currentIndex;
    }

    private int PreviousIndex(int currentIndex)
    {
        currentIndex--;
        int maxIndex = MaxIndex();
        if (currentIndex < 0) return maxIndex;

        return currentIndex;
    }

    private void UIUpdate()
    {
        SettingUIInfo(PreviousIndex(currentIndex), 0, reduceAlpha);
        SettingUIInfo(NextIndex(currentIndex), 2, reduceAlpha);
        //SettingUIInfo(currentIndex, 1, IsHasItem() ? 1.0f : reduceAlpha);
        SettingUIInfo(currentIndex, 1, 1.0f);
    }

    //現在選択中のアイテムが個数があるかどうか
    private bool IsHasItem()
    {
        return IsHasItem(itemList[currentIndex]);
    }

    private bool IsHasItem(ItemPair item)
    {
        return item.num > 0;
    }
}
