﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageSliderCtrl : MonoBehaviour
{
    [Range(0.001f, 1.0f)]
    [Header("ダメージSliderが減っていくスピード")]
    [SerializeField]
    private float damageSpeed = 0.5f;

    //自身のスライダーComponent
    private Slider mySlider;

    //自身の親のスライダーComponent
    private Slider parentSlider;

	// Use this for initialization
	void Start ()
    {
        mySlider = GetComponent<Slider>();
        parentSlider = transform.parent.GetComponent<Slider>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //自身のスライダーを親のスライダーに近づける処理
        Utility.CustomFunc.CheckAction(IsOverMySlider, ClosingSliderValue);
	}

    //回復するとき呼ばれる関数
    public void HealEvent()
    {
        //スライダーvalueの情報を親のものにする
        SetSameSliderValue();
    }

    //スライダーvalueの情報を親のものにする
    private void SetSameSliderValue()
    {
        mySlider.value = parentSlider.value;
    }

    //親のスライダーvalueが自身のスライダーvalueより大きいかどうか
    private bool IsOverParentSlider()
    {
        return parentSlider.value > mySlider.value;
    }

    //子のスライダーvalueが親のスライダーvalueより大きいかどうか
    private bool IsOverMySlider()
    {
        return mySlider.value > parentSlider.value;
    }

    //自身のスライダーvalueを親のスライダーvalueに近づける
    private void ClosingSliderValue()
    {
        //自身のスライダーvalueを更新する
        mySlider.value -= SubSliderValue();

        //親のスライダーvalueのほうが大きかったら
        if (IsOverParentSlider())
        {
            //親のスライダーvalueに合わせる
            SetSameSliderValue();
        }
    }

    //自身のスライダーvalueから引かれる値
    private float SubSliderValue()
    {
        float fpsRate = Time.deltaTime;
        return fpsRate * damageSpeed;
    }
}
