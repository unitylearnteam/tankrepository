﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Rigidbody))]
public class PhysicsAffected : MonoBehaviour
{
    [SerializeField]
    private float m_UpwardsModifier;
    private Rigidbody m_Rigidbody;

	// Use this for initialization
	void Awake ()
    {
        m_Rigidbody = GetComponent<Rigidbody>();	
	}
	
	// Update is called once per frame
	public void ApplyForce (float force, Vector3 position, float radius)
    {
        Debug.Log("Enter Knockback");
        m_Rigidbody.AddExplosionForce(force, position, radius, m_UpwardsModifier);
	}
}
