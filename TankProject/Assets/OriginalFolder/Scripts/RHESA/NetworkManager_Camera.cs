﻿/*************************************
 * 制作者  ：シスワントレサ
 * 制作日  ：2018年10月29日
 * 内容     ：カメラの移動
 * ***********************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkManager_Camera : NetworkManager {

    [Header("Scene Camera Properties")]
    [SerializeField] Transform sceneCamera; // the scene camera
    [SerializeField] float cameraRotationRadius = 24f;  // the radius of the camera's rotation
    [SerializeField] float cameraRotationSpeed = 3f; // the speed of the camera's rotation
    [SerializeField] bool canRotate = true; // can the camera be rotated;

    float rotation; // current rotation of the camera

    private void Start()
    {
        sceneCamera = Camera.main.transform;
    }

    public override void OnStartClient (NetworkClient client)
    {
        canRotate = false;
    }

    public override void OnStartHost()
    {
        canRotate = false;
    }

    public override void OnStopClient()
    {
        canRotate = true;
    }

    public override void OnStopHost()
    {
        canRotate = true;
    }

    // Update is called once per frame
    void Update ()
    {
        if (!canRotate) return;

        rotation += cameraRotationSpeed * Time.deltaTime;
        if (rotation >= 360f)
            rotation -= 360f;

        // Rotate the camera around the center of the scene
        sceneCamera.position = Vector3.zero;
        sceneCamera.rotation = Quaternion.Euler(0f, rotation, 0f);
        sceneCamera.Translate(0f, cameraRotationRadius, -cameraRotationRadius);
        sceneCamera.LookAt(Vector3.zero);
	}
}
