﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public Transform[] spawnPoint;
    public GameObject[] itemPrefab;
    private GameObject[] item;

    private Utility.Timer timer;

    // Use this for initialization
    void Start()
    {
        timer = new Utility.Timer();
        timer.Initialize(5f);

        item = new GameObject[spawnPoint.Length];
    }

    // Update is called once per frame
    void Update()
    {
        timer.Update();
        if (!timer.IsTime()) return;
        timer.Reset();
        int rnd = Random.Range(0, item.Length);
        if (item[rnd] != null) Destroy(item[rnd]);
        item[rnd] = Instantiate(itemPrefab[Random.Range(0, itemPrefab.Length)], spawnPoint[rnd]) as GameObject;
    }


}
