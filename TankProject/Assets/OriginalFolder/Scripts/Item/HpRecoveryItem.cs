﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpRecoveryItem : MonoBehaviour
{
    [SerializeField]
    GameObject itemUI;

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        itemUI.GetComponentInChildren<ItemUIList>().AddItem(ItemType.HP_RECOVERY);
        Destroy(gameObject);
    }

}
