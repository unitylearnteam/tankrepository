﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponItem : MonoBehaviour
{
	private WeaponType weapon;

	void Start()
	{
		weapon = WeaponType.ThreeShot;		
	}

	void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
		other.GetComponent<TankWeapon>().AddWeapon(weapon);
        Destroy(gameObject);
    }
}