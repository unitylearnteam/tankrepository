﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//自身が何のアイテムか、を伝えるComponent
public class ItemSender : MonoBehaviour
{
    [SerializeField]private ItemType type = ItemType.NONE;

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        if (type == ItemType.NONE) return;

        //ItemListに対応したアイテムを格納する
        ItemUIList itemList = other.GetComponentInChildren<ItemUIList>();
        itemList.AddItem(type);

        DestroyObject(gameObject);
    }
}
