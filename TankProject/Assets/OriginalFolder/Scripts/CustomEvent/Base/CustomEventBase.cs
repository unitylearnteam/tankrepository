﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CustomEventBase : MonoBehaviour
{

    [SerializeField] protected List<CustomEventBase> nextEvents;

	protected void EventEnd()
    {
        enabled = false;
        nextEvents.ForEach(e => e.enabled = true);
    }
}


public class TriggerEventBase : CustomEventBase { }
public class ActionEventBase : CustomEventBase
{
    private void Awake()
    {
        enabled = false;
    }
}
