﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Action_FadeOut : ActionEventBase
{
    [SerializeField] private float time;
    [SerializeField] private Image image;

    void OnEnable()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 1.0f);
    }

    void FixedUpdate()
    {
        Color currentColor = image.color;
        currentColor.a -= 1.0f / 60.0f / time;
        image.color = currentColor;

        if(image.color.a < 0) EventEnd();
    }
}
