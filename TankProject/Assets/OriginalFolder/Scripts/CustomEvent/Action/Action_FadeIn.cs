﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Action_FadeIn : ActionEventBase
{
    [SerializeField] private float time;
    [SerializeField] private Image image;

    [SerializeField]
    private float maxAlpha = 1f;

    private void OnEnable()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0.0f);
    }

    void FixedUpdate()
    {
        Color currentColor = image.color;
        currentColor.a += 1.0f / 60.0f / time;
        image.color = currentColor;

        if(image.color.a > maxAlpha)
        {
            EventEnd();
            Color color = image.color;
            color.a = maxAlpha;
            image.color = color;
        }
	}
}
