﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//自身のプレイヤーが〇Pであるか
public enum PlayerType
{
    P1,    //1P
    P2,    //2P
    P3,    //3P
    P4,    //4P
    NONE,  //プレイヤーなし
}

public class PlayerManager : MonoBehaviour
{
    private static PlayerManager instance;
    private PlayerType type;

    public static PlayerManager Instance
    {
        get { return instance; }
        set { instance = value; }
    }

    public PlayerType PlayerType
    {
        get { return type; }
        set { type = value; }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            type = PlayerType.NONE;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
