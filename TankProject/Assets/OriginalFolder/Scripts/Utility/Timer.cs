﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
    public class Timer
    {

        private float setTime = 10f;
        private float currentTime;

        public void Update()
        {
            currentTime -= Time.deltaTime;
        }

        public void Initialize(float second)
        {
            setTime = second;
            currentTime = setTime;
        }

        public bool IsTime()
        {
            return (currentTime <= 0);
        }

        public void Reset()
        {
            currentTime = setTime;
        }
    }
}