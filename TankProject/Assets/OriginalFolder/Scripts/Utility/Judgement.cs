﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
    struct Judgement
    {
        //レイヤーとの比較
        public static bool IsCompareLayer(string layerName, GameObject gameObject)
        {
            return LayerMask.LayerToName(gameObject.layer) == layerName;
        }

        //タグとの比較
        public static bool IsCompareTag(string tagName, GameObject gameObject)
        {
            return tagName == gameObject.tag;
        }
    }
}