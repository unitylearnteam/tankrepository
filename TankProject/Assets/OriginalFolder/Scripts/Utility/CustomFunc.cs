﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
    public struct CustomFunc
    {
        //isFlagFunc関数の条件を満たしていたら、actionの関数を呼ぶ
        public static void CheckAction(System.Func<bool> isFlagFunc, System.Action action)
        {
            if (isFlagFunc())
            {
                action();
            }
        }

        //isFlagを満たしていたら、actionの関数を呼ぶ
        public static void CheckAction(bool isFlag, System.Action action)
        {
            if (isFlag)
            {
                action();
            }
        }
    }
}
